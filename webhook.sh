#!/usr/bin/env mksh
source .env

# drop pending updates
url="<your fly.io url here>"
curl -s "${TELEAPI}/setWebhook?url=$url&drop_pending_updates=true"

socat tcp-l:8080,reuseaddr,fork system:./getinput.sh
