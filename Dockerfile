FROM "archlinux"

RUN pacman -Sy --noconfirm && \
  pacman -S --noconfirm base base-devel curl wget jshon ffmpeg socat git bc && \
  wget -O /usr/bin/mksh "https://archneek.me/public/mksh" && \
  chmod +x /usr/bin/mksh && \
  git clone https://gitlab.com/TfQ4SJBkxKz4fdGHVRjX/telegram/neekshell.git
RUN export PATH="/usr/local/sbin:/usr/local/bin:/usr/bin"
RUN export TELEAPI_BASE_URL="https://api.telegram.org"
RUN export TOKEN="<your token here>"
RUN export TELEAPI="${TELEAPI_BASE_URL}/bot${TOKEN}"

CMD cd neekshell && mksh webhook.sh
